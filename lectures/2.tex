\section{Radicals}

We now want to find an equivalent characterisation of $Z(I) = Z(J)$ for two ideals $I,J$ of $R$.

\begin{defn}
  Let $I$ be an ideal. The \emph{radical} \index{radical} of $I$ is
  \[
  \sqrt{I} \defined \lset x\in R\ssp \text{there exists an }n>0\text{ such that } x^n\in I\rset.
  \]
\end{defn}
\begin{mdefn}
  An element $x\in R$ is called \emph{nilpotent} \index{nilpotent element} if there is an $n>0$ such that $x^n=0$.
\end{mdefn}
\begin{mexample}
  \leavevmode
  \begin{enumerate}
    \item The zero element is always nilpotent.
    \item In an integral domain, there are no non-zero nilpotent elements.
  \end{enumerate}
\end{mexample}
\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item $\sqrt{I}$ is an ideal of $R$.
    \item $I \sse \sqrt{I} = \sqrt{\sqrt{I}}$.
    \item $\sqrt{I} = R$ if and only if $I=R$.
    \item $R/\sqrt{I}$ has no non-zero nilpotent elements.
  \end{enumerate}
\end{lem}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Let $x,y\in \sqrt{I}$ and $m,n>0$ such that $x^m,y^n\in I$. Then
    \[
      \left(x+y\right)^{m+n-1} = \sum_{i=0}^{m+n-1}\binom{m+n-1}{i}x^iy^{m+n-1-i}.
    \]
    Now by assumption $x^i\in I$ for $i\geq m$ and $y^{m+n-1-i}\in I$ for $i<m$. So the whole sum is in $I$, and hence $x+y\in \sqrt{I}$. Furthermore, we have for any $r\in R$
    \[
    (rx)^m = r^mx^m
    \]
    which is in $I$.
  \item By setting $n=1$, we get $I\sse \sqrt{I}$, and hence $\sqrt{I} \sse \sqrt{\sqrt{I}}$. For the reverse inclusion, let $x\in \sqrt{\sqrt{I}}$ and $n>0$ such that $x^n\in \sqrt{I}$. Then there is a $m>0$ such that $x^{nm}=(x^n)^m\in I$, which implies $x\in \sqrt{I}$.
    \item As $1^n=1$ for all $n>0$, $1\in I$ if and only if $1\in \sqrt{I}$.
    \item Let $z\in R/\sqrt{I}$ with $z^n=0$. Then $z^n\in \sqrt{I}$, so $z\in \sqrt{\sqrt{I}} = \sqrt{I}$, which is equivalent to $z=0$.
  \end{enumerate}
\end{proof}

\begin{defn}
  \leavevmode
  \begin{enumerate}
  \item An ideal $I$ is a \emph{radical ideal} \index{ideal!radical} if $I = \sqrt{I}$ holds.
  \item $\nilr R\defined \sqrt{\genby{0}}$ is the \emph{nilradical} \index{nilradical}\index{radical!nil-}.
  \item If $R$ has no non-zero nilpotent elements then $R$ is \emph{reduced} \index{ring!reduced}.
  \end{enumerate}
\end{defn}
\begin{mexample}\label{2: nilradical of quotient}
  Let $I\sse R$ be an ideal and $\pi:R\to R/I$ the canonical projection. Then the nilradical of $R/I$ is given by
  \begin{align*}
    \nilr R/I &= \lset \overline{x}\in R/I\ssp \overline{x}^n = 0 \text{ for a n }>0\rset \\
              &= \lset \pi(x)\ssp x\in R,x^n\in I\text{ for a n }>0\rset\\
              &= \pi \left(\sqrt{I}\right).
  \end{align*}
\end{mexample}
\begin{lem}
  An ideal $I$ is a radical ideal if and only if $R/I$ is reduced.
\end{lem}
\begin{proof}
  We have the following equivalences:
  \begin{align*}
    I\text{ is a radical ideal} &\iff \text{for all }x\in R,~n>0\text{ with }x^n \in I \text{ it holds that }x\in I\\
                                &\iff \text{in }R/I:~\overline{x}^n=0\text{ implies }\overline{x}=0\\
                                &\iff R/I \text{ is reduced}.
  \end{align*}
\end{proof}

\begin{defn}
  We call $R_{\redr} \defined R/\nilr R$ the \emph{reduced ring associated to} $R$.
\end{defn}
\begin{example}\label{2: radical in z}
  Let $R=\zz$ and $I=\genby{a}$ for an $a\in \zz$. How does $\sqrt{\genby{a}}$ look like?
  Consider the decomposition into prime factors
  \[
  a = p_1^{m_1}\cdot \hdots \cdot p_l^{m_l}.
  \]
  Then $\sqrt{\genby{a}} = \genby{p_1 \hdots p_l}$:\par
  If $x\in \sqrt{\genby{a}}$, then there is a $n>0$ such that $x^n\in \genby{a}$. So $a$ divides $x^n$, and hence $p_1,\hdots p_l$ divide $x$, which implies $x\in \genby{p_1 \hdots p_l}$. Let now $x\in \genby{p_1 \hdots p_l}$. Choose $n\geq \max \lset m_1,\hdots, m_l\rset$.
  Then $x^n\in \genby p_1^n\hdots p_l^n\sse \genby{p_1^{m_1}\hdots p_l^{m_l}} = \genby{a}$ which implies $a\in \sqrt{a}$.
\end{example}

\begin{prop}
  For any ideal $I$
  \[
  \sqrt{I} = \bigcap_{\substack{\idp\text{ prime}\\I\sse \idp}} \idp
  \]
  holds.
\end{prop}

\begin{proof}
  Let $x\in \sqrt{I}$ and $n>0$ such that $x^n\in I$. Let $\idp$ be a prime ideal with $I\sse \idp$. Then $x^n\in \idp$, and as $\idp$ is a prime ideal, this already implies $x\in \idp$.\par
  For the converse, assume that $x \in \bigcap_{\substack{\idp,\\I\sse \idp}}\idp$ and that $x\notin \sqrt{I}$. We now want to use Zorn's Lemma in a non-obvious way to arrive at a contradiction. For that, define
  \[
  \Sigma \defined
  \lset J \subset R \;\middle|\;
  \begin{tabular}{@{}l@{}}
    $J$ is an ideal of $R$,\\
    for all $n>0$: $x^n\notin J$.
   \end{tabular}
   \rset.
   \]
   First note that $I\in \Sigma$, as $x\notin \sqrt{I}$. So $\Sigma \neq \emptyset$. Furthermore, $\Sigma$ is partially ordered by inclusion. Let $\left(J_t\right)_{t\in T}$ be a non-empty chain in $\Sigma$ and consider
   \[
   \tilde{J}\defined \bigcup_{t\in T}J_t.
   \]
   Then $\tilde{J}$ contains $I$, is an ideal\footnote{Note that unions of ideals are not necessarily ideals.} and does not contain $x^n$ for all $n>0$. So $\tilde{J}$ is an upper bound of $\left(J_t\right)_{t\in T}$. By Zorn's Lemma, this implies that $\Sigma$ contains a maximal element $\tilde{\idp}$. We now show that $\tilde{\idp}$ is a prime ideal:\par
   Let $a,b\in R\setminus \tilde{p}$. Then $\genby{a}+\tilde{p},\genby{b}+\tilde{p}$ strictly contains $\tilde{p}$ and hence cannot be in $\Sigma$. By definition of $\Sigma$, there are now $m,n>0$ such that $x^m\in \genby{a}+\tilde{p}$ and $y^n\in \genby{b}+\tilde{p}$.
   So there are $c,d\in R$ and $r,s\in \tilde{\idp}$ such that $x^m = ac+r$ and $x^n = bd + s$. Now
   \begin{align*}
     x^{m+n} &= \left(ac+r\right)\cdot \left(bd+s\right)\\
     &= \underbrace{abcd}_{\in \genby{ab}} + \underbrace{rbd+sac+rs}_{\in \tilde{\idp}},
   \end{align*}
   so $x^{n+m}\in \genby{ab}+\tilde{\idp}$. If $ab$ would be an element of $\tilde{\idp}$, then $\genby{ab}\sse \tilde{\idp}$, which would imply $x^{n+m}\in \tilde{\idp}$. Therefore $ab$ cannot be an element of $\tilde{\idp}$, which is equivalent to $\tilde{\idp}$ being prime.\\\par
   But by the original assumption, $x \in \bigcap_{\substack{\idp\\I\sse \idp}}\idp$, and hence $x\in \tilde{\idp}$. This is a contradiction as $\tilde{\idp}$ is an element of $\Sigma$.
\end{proof}

\begin{cor}
\leavevmode
\begin{enumerate}
  \item The nilradical of $R$ is given the intersection of all prime ideals of $R$; i.e.
 \[
 \nilr R= \bigcap_{\idp \text{ prime}} \idp.
 \]
 \item[ii)\textsuperscript{\extrasymbol}] The canonical projection $\pi:R \to R/\nilr R$ induces a homeomorphism
 \[
 \pi^{\#}:
 \begin{tikzcd}
 \spec R\ar{r}[above]{\sim}&
 \spec \left(R/\nilr R\right).
 \end{tikzcd}
 \]
 \end{enumerate}
\end{cor}
\begin{proof}
  The spectrum of the quotient by $\nilr R$ is given by
  \begin{align*}
    \spec R/\nilr R &= \lset \text{prime ideals of }R/\nilr R\rset\\
                    &= \lset \pi\left(\idp\right)\ssp \idp\text{ prime},~\nilr R \sse \idp \rset\\
                    &= \lset \pi \left(\idp\right)\ssp \idp \text{ prime}\rset.
  \end{align*}
\end{proof}

\begin{mexample}
  Consider $R\defined \zz/a\zz$. Then the nilradical is given by
  \[
  \bigcap_{\substack{p\text{ prime}\\p\divd a}}\genby{\overline{p}} = \genby{\overline{p_1\hdots p_m}},
  \]
  where $a = p_1^{m_1}\cdot\hdots\cdot p_l^{m_l}$. Note that this recovers the results of \cref{2: nilradical of quotient} and \cref{2: radical in z}.
\end{mexample}

\begin{cor}\label{2: zeros of radical are zeros of ideal}
For any ideal $I$ of $R$, $Z(I) = Z\left(\sqrt{I}\right)$ holds.
\end{cor}
\begin{proof}
  As $I \sse \sqrt{I}$, $Z\left(\sqrt{I}\right) \sse Z(I)$ follows (c.f. \cref{1: zi basic props}). On the other hand, as
  \[
  \sqrt{I} = \bigcap_{\substack{\idp ~\text{prime}\\I\sse \idp}},
  \]
  every prime ideal that contains $I$ also contains $\sqrt{I}$. So $Z(I) = Z\left(\sqrt{I}\right)$.
\end{proof}

\begin{cor}
  For all ideals $I,J$ the following holds:
  \begin{enumerate}
    \item $Z(J)\sse Z(I)$ if and only if $\sqrt{J} \supseteq \sqrt{I}$.
    \item $Z(I) = Z(J)$ if and only if $\sqrt{I} = \sqrt{J}$.
  \end{enumerate}
\end{cor}
\begin{proof}
  By symmetry, it suffices to prove i). If $\sqrt{I}\sse \sqrt{J}$, then by \cref{1: zi basic props} $Z\left(\sqrt{I}\right)\supseteq Z\left(\sqrt{J}\right)$ holds.
  \Cref{2: zeros of radical are zeros of ideal} now implies $Z(I)\supseteq Z(J)$.\par
  For the other direction, assume $Z(J) \sse Z(I)$. Then every prime ideal that contains $J$ also contains $I$. Now
  \begin{align*}
  \sqrt{I}  &= \bigcap_{\substack{\idp~\text{prime}\\I\sse \idp}} \idp \\
            &\sse \bigcap_{\substack{\idp~\text{prime}\\ J \sse \idp}}\idp = \sqrt{J}.
  \end{align*}
\end{proof}
We now introduce a notion similar to the nilradical, but for maximal ideals:
\begin{defn}
  The \emph{Jacobson radical}\index{radical! jacobson}\index{Jacobson radical} $\jacr R$ is defined as the intersection of all maximal ideals of $R$; i.e.
  \[
  \jacr R \defined \bigcap_{\idm~\text{maximal}} \idm.
  \]
\end{defn}
\begin{prop}
  The Jacobson radical of $R$ is given by
  \[
  \jacr R = \lset x\in R\ssp 1-ax\in R\unts \text{ for all }a\in R\rset.
  \]
\end{prop}
\begin{proof}\leavevmode
\begin{itemize}
    \item[] \glqq $\subseteq$ \grqq: Let $x\in \jacr R$ and $a\in R$. Assume $1-ax$ is not a unit in $R$. Then by \cref{1: existence of maximals} there is a maximal ideal $\idm$ containing $1-ax$. But then
    \[
    1 = \left(1-ax\right) + ax\]
    As both summands are in $\idm$, $1\in \idm$ would follow, which is a contradiction.\\
    \item[] \glqq $\supseteq$ \grqq: Let $x\in R$ be an element such that $1-ax$ is a unit for every $a\in R$, and let $\idm$ be a maximal ideal that does not contain $x$. Then
    $\genby{x}+\idm = \genby{1}$, so there is a $a\in R$ and $y\in \idm$ such that $1=ax+y$. But then $y=1-ax$ would be a unit, which is not possible (as $y\in \idm$).
\end{itemize}
\end{proof}
\begin{defn}
  A ring $R$ is \emph{local} if $R$ contains exactly one maximal ideal\index{ring! local}\index{local! ring}.
\end{defn}
\begin{mexample}
  \leavevmode
  \begin{enumerate}
    \item Every field is a local ring, with maximal ideal $\genby{0}$.
    \item For every prime number $p$ and every $n\geq 1$, $R\defined \zz/p^n\zz$ is a local ring: The prime ideals in $R$ are in one-to-one, order preserving correspondence with the prime ideals in $\zz$ that contain $\genby{p^n}$. But the only prime ideal that contains $\genby{p^n}$ is $\genby{p}$. So $R$ has only one prime ideal, given by $\genby{\overline{p}}$, which has to be maximal.
    \item Let $\idp\sse R$ be a prime ideal, $S\defined R\setminus \idp$ and the localization $R_{\idp}\defined S^{-1}R$. Then $R_{\idp}$ is a local ring:\par
    Consider the ideal
    \[
    I \defined \lset \frac{a}{s}\ssp a\in \idp, s\in S\rset .
    \]
    Let $b/t\notin I$. Hence $b\notin \idp$, which implies $b\in S$. So $b/t$ is a unit in $R_{\idp}$. This shows that every ideal $J$ with $J\ssne I$ contains a unit.
  \end{enumerate}
\end{mexample}

\begin{lem}
  Let $I\ssne R$ be an ideal. Then the following are equivalent:
  \begin{enumerate}
    \item $R$ is a local ring with maximal ideal $I$;
    \item $R\setminus I \sse R\unts$;
    \item $R\setminus I = R\unts$.
  \end{enumerate}
\end{lem}
\begin{proof}
  This follows from  \cref{1: existence of maximals}.
\end{proof}
\lec
