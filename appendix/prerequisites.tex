\chapter{Prerequisites}
We recall some basic definitions, as introduced in \cite{schroer}.
\section{Rings}
\begin{defn}
  A \toidx{ring} is a tupel $(R,+,\cdot)$, consisting of a set $R$, and maps
  \[
  +:R\times R\to R,~(a,b)\mapsto a+b\]
  and
  \[
  \cdot: R\times R\to R, (a,b)\mapsto a\cdot b
  \]
  such that the following holds for all $a,b,c\in R$
  \begin{itemize}
  \item[(A1)] $a+\left(b+c\right) = \left(a+b\right)+c$,
  \item[(A2)] $a+b = b+a$,
  \item[(A3)] there is an element $0=0_R\in R$ such that $a+0 = a$,
  \item[(A4)] there is an element $-a\in R$ such that $a+\left(-a\right)=0$,
  \item[(R1)] $a\cdot(b\cdot c) = (a\cdot b)\cdot c$,
  \item[(R2)] there is an element $1=1_R\in R$ such that $1\cdot a = a\cdot 1 = a$,
  \item[(M)] $(a+b)\cdot c = (a\cdot c)+(b\cdot c)$.
  \end{itemize}
  The maps $+$ and $\cdot$ are often ommited in notation. The short-hand notation for the multiplication is $ab \defined a\cdot b$.
\end{defn}
\begin{defn}
  Let $R$ be a ring.
  \begin{enumerate}
    \item $R$ is \emph{commutative} \index{ring!commutative} if $ab = ba$ for all $a,b\in R$.
    \item An element $r\in R$ is a \toidx{unit} of $R$ if there is another element $s\in R$ such that $sr = rs = 1_R$.
    \item A ring in which every element is a unit is a called a \emph{skew-field} \index{skew-field}\index{field!skew-}. A commutative skew-field is a called a \emph{field} \index{field}.
  \end{enumerate}
\end{defn}
\begin{mconvention}
From now on, all rings are assumed to be commutative.
\end{mconvention}
\begin{defn}
  Let $R$ be a ring.
  \begin{enumerate}
    \item A subset $U\sse R$ is a \emph{subring}\index{ring! sub-} of $R$ if
    \begin{enumerate}[label = (\arabic*)]
      \item $1\in U$,
      \item for all $u_1,u_2\in U$, $u_1u_2\in U$ holds.
    \end{enumerate}
    \item A subset $\ida\sse R$ is an \toidx{ideal} of $R$ if
    \begin{enumerate}[label = (\arabic*)]
      \item $(\ida,+)$ is an additive subgroup of $(R,+)$,
      \item for all $r\in R$ and $a\in \ida$, $ra\in \ida$ holds.
    \end{enumerate}
    \item An ideal $\idp$ of $R$ is a \emph{prime ideal} \index{ideal!prime} if for all $a,b\in R$ with $ab\in \idp$ already $a\in \idp$ or $b\in \idp$ follows.
    \item An ideal $\idm$ of $R$ is a \emph{maximal ideal} \index{ideal!maximal} if $\idm \neq R$ and for all other ideals $\ida$ with $\idm \sse \ida \sse R$ already $\ida = \idm $ or $\ida = R$ follows.
  \end{enumerate}
\end{defn}
\begin{rem}
  Let $R$ be a ring and $\ida$  an ideal of $R$.
  \begin{enumerate}
  \item Consider the set of cosets of $\ida$, i.e. the set
  \[
  R/\ida \defined \lset r + \ida \ssp r \in R\rset.
  \]
  We can define a ring structure on $R/\ida$ by setting
  \[
  \left(r+\ida\right) + \left(s+\ida\right)\defined \left((r+s)+\ida\right)
  \]
  and
  \[
    \left(r+\ida\right) \cdot \left(s+\ida\right)\defined \left((rs)+\ida\right).
  \]
  \item The map
  \[
  \begin{tikzcd}
  \lset \idb\sse R \;\middle|\;
  \begin{tabular}{@{}l@{}}
    $\idb$ is an ideal of $R$,\\
    $\ida \sse \idb$.
   \end{tabular}
   \rset\ar{r}&
   \lset \text{ideals of }R/\ida\rset\\
   \idb \ar[mapsto]{r}&
   \idb + \ida
  \end{tikzcd}
  \]
  is a bijection. We will refer to this as \emph{ideal correspondence}.
  \end{enumerate}
  \end{rem}

  The following is a consequence of Zorn's Lemma and the ideal correspondence:
  \begin{prop}
    Let $0\neq R$ be a a ring.
    \begin{enumerate}
      \item There is a maximal ideal $\idm$ of $R$.
      \item Every ideal $\ida$ of $R$ is contained in some maximal ideal.
    \end{enumerate}
  \end{prop}

  \begin{mprop}
    Let $0\neq R$ be a ring and $\ida$ an ideal of $R$.
    \begin{enumerate}
      \item The following are equivalent:
      \begin{enumerate}[label = \alph*)]
        \item $\ida$ is a prime ideal.
        \item $R/\ida$ is an integral domain.
      \end{enumerate}
      \item The following are equivalent:
      \begin{enumerate}[label = \alph*)]
        \item $\ida$ is a maximal ideal.
        \item $R/\ida$ is a field.
      \end{enumerate}
    \end{enumerate}
  \end{mprop}


\subsection{Maps between rings}
\begin{defn}
  Let $R,S$ be rings. A map $f:R\to S$ is a \emph{ring homomorphism} \index{morphism! of rings} \index{ring! homomorphism} if it satisfies the following conditions:
  \begin{enumerate}[label= (\arabic*)]
    \item $f(a+b) = f(a) + f(b)$ for all $a,b\in R$,
    \item $f(ab) = f(a)f(b)$ for all $a,b\in R$,
    \item $f(1_R) = 1_S$.
  \end{enumerate}
\end{defn}

\begin{remdef}
  Let $R,S$ be rings and $f:R\to S$ be a ring homomorphism.
  \begin{enumerate}
  \item The \toidx{kernel} of $f$ is the subset
  \[
  \ker f \defined \lset r\in R\ssp f(r) = 0\rset = f^{-1}(0).
  \]
  It is an ideal of $R$.
  \item The \toidx{image} of $f$ is the subset
  \[
  \im f \defined   \lset s\in S \;\middle|\;
    \begin{tabular}{@{}l@{}}
      there exists an $ r\in R$,\\
       such that $f(r)=s$.
     \end{tabular}
     \rset
      = f\left(R\right).
  \]
  It is a subring of $S$.
  \item Let $\ida\sse \ker f$ be an ideal of $R$. Then there is a unique ring homomorphism $\overline{f}:R/\ida \to S$ such that the following diagram commutes:
  \[
  \begin{tikzcd}
    R\ar{r}{f}\ar{d}&
    S\\
    R/\ida\ar[ur,dashed]{ur}[below right]{\overline{f}}
    &
  \end{tikzcd}
  \]
  \item The map
  \[
  \begin{tikzcd}
    R/\ker f \ar{r}&
    \im f\\
    r + \ker f \ar{r}&
    f(r)
  \end{tikzcd}
  \]
  is a well-defined isomorphism of rings.
  \end{enumerate}
\end{remdef}
